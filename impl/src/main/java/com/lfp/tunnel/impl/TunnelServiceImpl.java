package com.lfp.tunnel.impl;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Supplier;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.jwk.AbstractJwkManager;
import com.lfp.joe.jwk.JwkRecord;
import com.lfp.joe.net.socket.socks.Sockets;
import com.lfp.joe.retrofit.CallLFP;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.ssh.core.SSHConnectRequest;
import com.lfp.tunnel.service.TunnelService;
import com.lfp.tunnel.service.mapping.FrontEndFilter;
import com.lfp.tunnel.service.mapping.FrontEndFilter.Builder;
import com.lfp.tunnel.service.mapping.TunnelType;
import com.maximeroussy.invitrode.WordGenerator;

import one.util.streamex.StreamEx;
import retrofit2.Call;

public class TunnelServiceImpl extends AbstractJwkManager implements TunnelService, Scrapable {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int RANDOM_SUB_DOMAIN_LENGTH = 10;
	private static final WordGenerator SUB_DOMAIN_GENERATOR = new WordGenerator();
	private final Scrapable _scrapable = Scrapable.create();

	private final String hostname;
	private final int sshPort;
	private Duration frontEndFilterTimeout;
	private List<JwkRecord> _jwkList;

	public TunnelServiceImpl(String hostname, int sshPort, Duration frontEndFilterTimeout) {
		super("tunnel-service-v2-" + normalizeHostname(hostname), frontEndFilterTimeout.dividedBy(2),
				frontEndFilterTimeout);
		this.hostname = normalizeHostname(hostname);
		Validate.isTrue(sshPort >= 0, "invalid ssh port:" + sshPort);
		this.sshPort = sshPort;
		this.frontEndFilterTimeout = frontEndFilterTimeout;
	}

	@Override
	public Call<String> frontEndFilterCreate(TunnelType tunnelType, Boolean randomSubDomain) {
		if (tunnelType == null)
			return CallLFP.error(new IllegalArgumentException("tunnel type is required"));
		var builder = FrontEndFilter.builder().tunnelType(tunnelType);
		if (Boolean.TRUE.equals(randomSubDomain)) {
			if (!tunnelType.isHostnameVisible())
				return CallLFP.error(
						new IllegalArgumentException("tunnel type does not support random sub domains:" + tunnelType));
			String hostname = String.format("%s.%s",
					SUB_DOMAIN_GENERATOR.newWord(RANDOM_SUB_DOMAIN_LENGTH).toLowerCase(), this.hostname);
			builder.hostname(hostname);
		}
		return CallLFP.success(() -> frontEndFilterCreateInternal(builder));
	}

	@Override
	public Call<String> frontEndFilterCreate(FrontEndFilter frontEndFilter) {
		if (frontEndFilter == null)
			return CallLFP.error(new IllegalArgumentException("front end filter is required"));
		var builder = FrontEndFilter.builder();
		// copy to include defaults and validation
		JodaBeans.copyToBuilder(frontEndFilter, builder);
		return CallLFP.success(() -> frontEndFilterCreateInternal(builder));
	}

	@Override
	public Call<SSHConnectRequest> sessionCreate() {
		return CallLFP.success(() -> sessionCreateInternal());
	}

	public String frontEndFilterCreateInternal(FrontEndFilter.Builder fefBuilder) throws IOException {
		configurePort(fefBuilder);
		configureHostname(fefBuilder);
		var jwtb = this.newJwtBuilder();
		if (frontEndFilterTimeout != null)
			jwtb.setExpiration(new Date(System.currentTimeMillis() + frontEndFilterTimeout.toMillis()));
		jwtb.putClaimData(fefBuilder.build());
		String jwt = jwtb.compact();
		return jwt;
	}

	private void configurePort(Builder fefBuilder) throws IOException {
		int port = JodaBeans.get(fefBuilder, FrontEndFilter.meta().port());
		if (port == 0)
			fefBuilder.port(Sockets.allocatePort());
	}

	private void configureHostname(Builder fefBuilder) {
		String hostname = JodaBeans.get(fefBuilder, FrontEndFilter.meta().hostname());
		if (Utils.Strings.isBlank(hostname))
			return;
		if (hostname.equals(this.hostname))
			return;
		Supplier<IllegalArgumentException> onError = () -> {
			throw new IllegalArgumentException("invalid hostname:" + hostname);
		};
		int index = Utils.Strings.lastIndexOf(hostname, "." + this.hostname);
		if (index <= 0)
			throw onError.get();
		String subdomain = hostname.substring(0, index);
		if (Utils.Strings.isBlank(subdomain))
			throw onError.get();
		if (Utils.Strings.contains(subdomain, "."))
			onError.get();

	}

	public SSHConnectRequest sessionCreateInternal() {
		return (SSHConnectRequest) SSHConnectRequest.builder().hostname(hostname).port(sshPort).build();
	}

	@Override
	public boolean isScrapped() {
		return _scrapable.isScrapped();
	}

	@Override
	public Scrapable onScrap(Runnable task) {
		return _scrapable.onScrap(task);
	}

	@Override
	public boolean scrap() {
		return _scrapable.scrap();
	}

	public String getHostname() {
		return hostname;
	}

	private static String normalizeHostname(final String hostname) {
		boolean mod = true;
		String input = hostname;
		while (input != null && mod) {
			mod = false;
			var leng = input.length();
			input = Utils.Strings.trim(input).toLowerCase();
			if (leng > input.length()) {
				mod = true;
				continue;
			}
			if (Utils.Strings.startsWithIgnoreCase(input, ".")) {
				input = input.substring(1);
				mod = true;
				continue;
			}
			if (Utils.Strings.endsWithIgnoreCase(input, ".")) {
				input = input.substring(0, input.length() - 1);
				mod = true;
				continue;
			}
		}
		Validate.isTrue(Utils.Strings.isNotBlank(input), "invalid hostname:%s", hostname);
		return input;
	}

	@Override
	protected Iterable<JwkRecord> read() throws IOException {
		if (this._jwkList == null)
			return List.of();
		return new ArrayList<>(_jwkList);
	}

	@Override
	protected void write(StreamEx<JwkRecord> jwks) throws IOException {
		this._jwkList = jwks.toImmutableList();
	}

	@Override
	protected <X> X lockAccess(ThrowingSupplier<X, Exception> accessor) throws Exception, InterruptedException {
		synchronized (this) {
			return accessor.get();
		}
	}

}
