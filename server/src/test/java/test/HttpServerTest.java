package test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.Executors;

import org.apache.sshd.agent.local.ProxyAgentFactory;
import org.apache.sshd.agent.unix.AprLibrary;
import org.apache.sshd.common.forward.PortForwardingEventListener;
import org.apache.sshd.common.keyprovider.KeyPairProvider;
import org.apache.sshd.common.session.Session;
import org.apache.sshd.common.session.SessionContext;
import org.apache.sshd.common.session.SessionHeartbeatController.HeartbeatType;
import org.apache.sshd.common.util.net.SshdSocketAddress;
import org.apache.sshd.server.ServerBuilder;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.auth.AsyncAuthException;
import org.apache.sshd.server.auth.password.PasswordAuthenticator;
import org.apache.sshd.server.auth.password.PasswordChangeRequiredException;
import org.apache.sshd.server.session.ServerSession;
import org.xnio.OptionMap;

import com.lfp.joe.threads.Threads;
import com.lfp.connect.undertow.ConnectionProperties;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.Undertows;
import com.lfp.connect.undertow.handler.LoadBalancingProxyClientLFP;
import com.lfp.connect.undertow.handler.ThreadHttpHandler;
import com.lfp.joe.utils.Utils;

import io.undertow.Handlers;
import io.undertow.client.UndertowClient;
import io.undertow.server.HttpHandler;

public class HttpServerTest {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args)
			throws InterruptedException, NoSuchMethodException, IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException, NoSuchAlgorithmException, IOException {
		{
			var serverBuilder = ServerBuilder.builder();
			SshServer server = serverBuilder.build();
			boolean aprInstalled = false;
			try {
				if (AprLibrary.getInstance() != null)
					aprInstalled = true;
			} catch (Exception ignore) {
				// ignored
			}
			logger.info("apr installed:" + aprInstalled);
			server.setAgentFactory(new ProxyAgentFactory());
			// System.out.println(server.getAgentFactory().getClass());
			server.setPort(22);
			// allow forwarding
			server.setSessionHeartbeat(HeartbeatType.IGNORE, Duration.ofSeconds(5));
			server.addPortForwardingEventListener(new PortForwardingEventListener() {

				@Override
				public void establishedExplicitTunnel(Session session, SshdSocketAddress local,
						SshdSocketAddress remote, boolean localForwarding, SshdSocketAddress boundAddress,
						Throwable reason) throws IOException {
					logger.info("establishedExplicitTunnel. session:{} local:{} remote:{}", session, local, remote);
				}

			});
			server.setPasswordAuthenticator(new PasswordAuthenticator() {

				@Override
				public boolean authenticate(String username, String password, ServerSession session)
						throws PasswordChangeRequiredException, AsyncAuthException {
					return true;
				}
			});
			// random hostkey provider
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(2048);
			KeyPair kp = kpg.generateKeyPair();
			server.setKeyPairProvider(new KeyPairProvider() {

				@Override
				public Iterable<KeyPair> loadKeys(SessionContext session) throws IOException, GeneralSecurityException {
					return Arrays.asList(kp);
				}
			});
			server.setForwardingFilter(new org.apache.sshd.server.forward.ForwardingFilter() {

				@Override
				public boolean canListen(SshdSocketAddress address, Session session) {
					return true;
				}

				@Override
				public boolean canConnect(Type type, SshdSocketAddress address, Session session) {
					return true;
				}

				@Override
				public boolean canForwardX11(Session session, String requestType) {
					return true;
				}

				@Override
				public boolean canForwardAgent(Session session, String requestType) {
					return true;
				}
			});
			server.start(); // start server
			logger.info("started ssh");
		}
	
		LoadBalancingProxyClientLFP proxyClient = new LoadBalancingProxyClientLFP((hsx, hosts) -> {
			if (hosts == null || hosts.length == 0)
				return null;
			if (hosts.length == 1)
				return hosts[0];
			return hosts[Utils.Crypto.getRandomInclusive(0, hosts.length - 1)];
		});
		if (true) {
			proxyClient.addHost(URI.create("http://localhost:8383"));
			proxyClient.addHost(URI.create("http://localhost:8484"));
		} else {
			proxyClient.addHost(URI.create("http://localhost:8282"));
			proxyClient.addHost(URI.create("http://localhost:8181"));
		}
		var proxyHandler = Handlers.proxyHandler(proxyClient);
		HttpHandler logHandler = hsx -> {
			System.out.println(UndertowUtils.getRequestURI(hsx));
			proxyHandler.handleRequest(hsx);
		};
		int threads = Utils.Machine.logicalProcessorCount();
		var threadHttpHandler = new ThreadHttpHandler(logHandler, Threads.Pools.centralPool().limit());
		var undertow = Undertows.serverBuilderBase().addHttpListener(8080, null).setHandler(threadHttpHandler).build();
		undertow.start();
		System.out.println(undertow.getListenerInfo());
		Thread.currentThread().join();
	}

}
