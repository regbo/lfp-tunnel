package com.lfp.tunnel.server;

import java.net.InetSocketAddress;
import java.net.URI;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.BiFunction;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.Undertows;
import com.lfp.connect.undertow.handler.DynamicProxyClient;
import com.lfp.connect.undertow.handler.ErrorLoggingHandler;
import com.lfp.connect.undertow.handler.ThreadHttpHandler;
import com.lfp.connect.undertow.retrofit.RetrofitHandler;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.ReadWriteAccessor;
import com.lfp.tunnel.server.config.TunnelServerConfig;
import com.lfp.tunnel.service.TunnelService;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.proxy.LoadBalancingProxyClient.Host;

public class HttpTunneler implements Scrapable {
	private final Scrapable _scrapable = Scrapable.create();
	private final ReadWriteAccessor<Set<TunnelRegistration>> tunnelRegistrations = new ReadWriteAccessor<>(
			new LinkedHashSet<>());
	private final String apiHostname;
	private final DynamicProxyClient proxyClient;
	private final Undertow server;

	public HttpTunneler(TunnelService tunnelService) {
		var cfg = Configs.get(TunnelServerConfig.class);
		var apiHandler = new RetrofitHandler<>(TunnelService.class, tunnelService);
		this.apiHostname = cfg.serviceSubDomain() + "." + cfg.serviceSubDomain();
		BiFunction<HttpServerExchange, Host[], Host> hostSelector = (hex, hosts) -> {
			var matchingTRs = tunnelRegistrations.readAccess(c -> {
				return Utils.Lots.stream(c).filter(v -> matchesHttp(v, hex)).toList();
			});
			var matching = Utils.Lots.stream(hosts).filter(h -> {
				var uri = h.getUri();
				var port = URIs.normalizePort(uri.getScheme(), uri.getPort());
				for (var tr : matchingTRs)
					if (tr.getBackEndPort() == port)
						return true;
				return false;
			}).toList();
			if (matching.isEmpty())
				return null;
			Host match;
			if (matching.size() == 1)
				match = matching.get(0);
			else
				match = matching.get(Utils.Crypto.getRandomInclusive(0, matching.size() - 1));
			return match;
		};
		this.proxyClient = new DynamicProxyClient() {

			@Override
			protected URI getUpstreamURI(HttpServerExchange exchange) {
				var matching = tunnelRegistrations.readAccess(c -> {
					return Utils.Lots.stream(c).filter(v -> matchesHttp(v, exchange)).toList();
				});
				if (matching.isEmpty())
					return null;
				TunnelRegistration match;
				if (matching.size() == 1)
					match = matching.get(0);
				else
					match = matching.get(Utils.Crypto.getRandomInclusive(0, matching.size() - 1));
				return URI.create("http://localhost:" + match.getBackEndPort());
			}
		};
		var loadBalanceProxyHandler = Handlers.proxyHandler(this.proxyClient);
		HttpHandler httpHandler = hex -> {
			var requestURI = UndertowUtils.getRequestURI(hex);
			if (apiHostname.equals(requestURI.getHost())) {
				apiHandler.handleRequest(hex);
				return;
			}
			loadBalanceProxyHandler.handleRequest(hex);
		};
		var errorLoggingHandler = new ErrorLoggingHandler(httpHandler);
		var threadHttpHandler = new ThreadHttpHandler(errorLoggingHandler);
		this.server = Undertows.serverBuilder(new InetSocketAddress("127.0.0.1", 0)).setHandler(threadHttpHandler)
				.build();
		this.server.start();
		this.onScrap(() -> this.server.stop());
	}

	private boolean matchesHttp(TunnelRegistration tunnelRegistration, HttpServerExchange httpServerExchange) {
		if (tunnelRegistration == null)
			return false;
		var fefilter = tunnelRegistration.getFrontEndFilter();
		if (Utils.Strings.isBlank(fefilter.getHostname()))
			return true;
		URI requestURI = UndertowUtils.getRequestURI(httpServerExchange);
		return Utils.Strings.equals(fefilter.getHostname(), requestURI.getHost());
	}

	public void addTunnelRegistration(TunnelRegistration tunnelRegistration) {
		this.tunnelRegistrations.writeAccess(c -> {
			c.add(tunnelRegistration);
		});
		tunnelRegistration.onScrap(() -> {
			this.tunnelRegistrations.writeAccess(c -> {
				c.remove(tunnelRegistration);
			});
		});
	}

	public String getApiHostname() {
		return apiHostname;
	}

	public int getPort() {
		InetSocketAddress inetSocketAddress = (InetSocketAddress) this.server.getListenerInfo().iterator().next()
				.getAddress();
		return inetSocketAddress.getPort();
	}

	@Override
	public boolean isScrapped() {
		return _scrapable.isScrapped();
	}

	@Override
	public Scrapable onScrap(Runnable task) {
		return _scrapable.onScrap(task);
	}

	@Override
	public boolean scrap() {
		return _scrapable.scrap();
	}

}
