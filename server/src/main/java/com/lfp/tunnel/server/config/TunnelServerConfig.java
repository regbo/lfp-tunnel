package com.lfp.tunnel.server.config;

import java.time.Duration;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.properties.converter.DurationConverter;

public interface TunnelServerConfig extends Config {

	@DefaultValue("1s")
	@ConverterClass(DurationConverter.class)
	Duration tlsHandshakeTimeout();

	@DefaultValue("0.0.0.0")
	String listenAddresss();

	String hostname();

	@DefaultValue("api")
	String serviceSubDomain();
	
	@DefaultValue("2222")
	int sshPort();
	
	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
	}
}
