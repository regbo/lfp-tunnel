package test;

import java.util.regex.Pattern;

import com.lfp.joe.serial.Serials;
import com.lfp.tunnel.service.mapping.FrontEndFilter;
import com.lfp.tunnel.service.mapping.TunnelType;

public class FrontEndFilterSerTest {

	public static void main(String[] args) {
		var fef = FrontEndFilter.builder().tunnelType(TunnelType.TCPS).hostname(" ").build();
		String json = Serials.Gsons.getPretty().toJson(fef);
		System.out.println(json);
		var fef2 = Serials.Gsons.get().fromJson(json, FrontEndFilter.class);
		System.out.println(fef2);
	}
}
