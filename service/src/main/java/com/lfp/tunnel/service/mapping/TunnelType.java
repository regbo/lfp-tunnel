package com.lfp.tunnel.service.mapping;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import com.lfp.joe.utils.Utils;

import io.mikael.urlbuilder.UrlBuilder;

public enum TunnelType {
	HTTP {
		@Override
		public String toScheme() {
			return "http";
		}

		@Override
		public int getDefaultPort() {
			return 80;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public boolean isHostnameVisible() {
			return true;
		}

		@Override
		public boolean isPathVisible() {
			return true;
		}
	},
	HTTPS {
		@Override
		public String toScheme() {
			return "https";
		}

		@Override
		public int getDefaultPort() {
			return 443;
		}

		@Override
		public boolean isSecure() {
			return true;
		}

		@Override
		public boolean isHostnameVisible() {
			return true;
		}

		@Override
		public boolean isPathVisible() {
			return true;
		}
	},
	TCP {
		@Override
		public String toScheme() {
			return "tcp";
		}

		@Override
		public int getDefaultPort() {
			return 80;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public boolean isHostnameVisible() {
			return false;
		}

		@Override
		public boolean isPathVisible() {
			return false;
		}
	},
	TCPS {
		@Override
		public String toScheme() {
			return "tcps";
		}

		@Override
		public int getDefaultPort() {
			return 443;
		}

		@Override
		public boolean isSecure() {
			return true;
		}

		@Override
		public boolean isHostnameVisible() {
			return true;
		}

		@Override
		public boolean isPathVisible() {
			return false;
		}
	};

	public abstract boolean isSecure();

	public abstract boolean isHostnameVisible();

	public abstract boolean isPathVisible();

	public abstract String toScheme();

	public abstract int getDefaultPort();

	
	public static Optional<TunnelType> parse(URI uri) {
		if (uri == null)
			return Optional.empty();
		return parse(uri.getScheme());
	}

	public static Optional<TunnelType> parse(String value) {
		if (Utils.Strings.isBlank(value))
			return Optional.empty();
		List<Function<TunnelType, String>> strFunctions = new ArrayList<>();
		strFunctions.add(TunnelType::toScheme);
		strFunctions.add(TunnelType::name);
		strFunctions.add(TunnelType::toString);
		for (Function<TunnelType, String> strFunction : strFunctions) {
			for (var tt : values()) {
				var str = strFunction.apply(tt);
				if (value.equalsIgnoreCase(str))
					return Optional.of(tt);
			}
		}
		return Optional.empty();
	}
}
