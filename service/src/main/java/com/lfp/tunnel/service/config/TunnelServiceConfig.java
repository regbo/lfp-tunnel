package com.lfp.tunnel.service.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.DefaultValue;

public interface TunnelServiceConfig extends Config {

	@DefaultValue("2222")
	int sshPortDefault();
}
