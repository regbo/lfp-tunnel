package com.lfp.tunnel.service;

import com.lfp.ssh.core.SSHConnectRequest;
import com.lfp.tunnel.service.mapping.FrontEndFilter;
import com.lfp.tunnel.service.mapping.TunnelType;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TunnelService {

	@GET("frontend-filter/create")
	Call<String> frontEndFilterCreate(@Query("type") TunnelType type,
			@Query("randomSubDomain") Boolean randomSubDomain);

	@POST("frontend-filter/create")
	Call<String> frontEndFilterCreate(@Body FrontEndFilter uri);

	@GET("session/create")
	Call<SSHConnectRequest> sessionCreate();

}
